#

class puppet_tmpdir (
        String $tmpdir
) {
        
  file { 'puppet_tmpdir':
    path   => $tmpdir,
    ensure => directory,
  }

}
